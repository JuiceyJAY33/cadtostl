from os import walk
import os

import sys
sys.path.append('C://Program Files//FreeCAD 0.19/bin')

import FreeCAD
import Part
import Mesh

f = []
dp =[]
dn =[]
mypath =  'C://Users//Justin Savage//PycharmProject//cadToStl3point8//step_files'
for (dirpath, dirnames, filenames) in walk(mypath):
    f.extend(filenames)
    dp.extend(dirpath)
    dn.extend(dirnames)
    break

print(f)
print(dp)
print(dn)


for file in f:
    print(file)
    path_use = mypath + '//' + file
    print(path_use)
    shape = Part.Shape()
    shape.read(path_use)
    doc = App.newDocument('Doc')
    pf = doc.addObject("Part::Feature", "{}".format(file))
    pf.Shape = shape
    name = os.path.splitext(file)[0]
    print(name)
    Mesh.export([pf], '{}.stl'.format(name))
