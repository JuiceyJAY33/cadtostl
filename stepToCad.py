import sys
sys.path.append('C://Program Files//FreeCAD 0.19/bin')

import FreeCAD
import Part
import Mesh
shape = Part.Shape()
shape.read('my_shape.STEP')
doc = App.newDocument('Doc')
pf = doc.addObject("Part::Feature","MyShape")
pf.Shape = shape
Mesh.export([pf], 'my_shape.stl')